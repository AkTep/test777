<?php 
 require'databaselk.php'?>


 <!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link
            rel="shortcut icon"
            type="image/x-icon"
            href="images/icons/icon.png"
        />
        <script src="js/jquery-3.5.0.min.js"></script>
        <script src="bootstrap/bootstrap.min.js"></script>
        <link rel="stylesheet" href="bootstrap/bootstrap.min.css" />
        <link rel="stylesheet" href="style.css" />
        <title>Film@Critic</title>
    </head>
    <body>
    	<!-- Баннер, поиск фильмов, авторизация--> 
        <nav class="navbar navbar-expand-lg navbar-light bg-light mynavbar">
            <a class="navbar-brand" href="#">Film@Critic</a>
            <a id="clock" class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true"></a>
            <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <form class="form-inline my-2 my-lg-0 ml-auto">
                    <input
                        class="form-control mr-sm-2"
                        type="search"
                        placeholder=""
                        aria-label="Search"
                    />
                    <button
                        class="btn btn-outline-success my-2 my-sm-0"
                        type="submit"
                    >
                        Поиск
                    </button>
                </form>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Регистрация </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">Вход</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="lk">
        	<?php ;?>
            <center><div class="tittle"></center>
                <h2><font color="black">Сведения о пользователе</font></h2> 
                <?php $user=get_users_by_id($_GET['id']);?>
           
            <p> Ваш логин:<?php echo $user["login"];?></p>
            <p>Ваш E-mail: <?php echo $user["email"];?></p>
            <p>Дата регистрации:  <?php echo $user["data"];?> </p>
            <p>О пользователе:  <?php echo $user["info"];?></p>
           <div class="avatar col-1">
            <p>Аватарка</p>
                                    <img
                                        class="rounded-circle"
                                        img src="images/avatar.jpg"
                                        alt=""
                                        width="64px"
                                        height="64px"
                                    />
            </div>

                                <a href="">Мои рецензии</a>
                                        <p>Ранг</p> 
                                        <hr></hr>                             
                                    <h2><font color="black">Статистика</font></h2>
                                    <p>Средняя оценка рецензий</p>
                                    <p>Средняя оценка фильмов</p>
                                    <p>средняя оценка других рецензий</p>
                                    <p>Частота публикаций в месяц</p>
                                    <a href="">Настройки</a>
</div>
 <footer class="mt-3">
                <p class="pl-3 pt-2">Все права защишены © 2020</p>
            </footer>
        </div>
        
        <script src="js/clock.js"></script>
    
    </body>
</html>
